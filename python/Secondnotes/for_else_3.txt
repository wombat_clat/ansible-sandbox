



# for and while <-- structured constructs RATFOR
# if  and   goto <-- more flexible and more powerful

for (i=0 ; i<n ; i++) {
    f(i);
    if (g(i)) {
        h(i);
        break
    }
    if (k(i)) {
        j(i);
        continue;
    }
    l(i);
} else {                 /* nobreak */
        m(i);
}
n(i);


i=0;
while (i<n) {
    f(i);
    if (g(i)) {
        h(i);
        break
    }
    if (k(i)) {
        j(i);
        i++;
        continue;
    }
    l(i);
    i++;
} else {
        m(i);
}
n(i);


i=0
top:
    if (i<n) {
        f(i);
        if (g(i)) {
            h(i);
            goto bottom;
        }
        if (k(i)) {
            j(i);
            i++;
            goto top;
        }
        l(i);
        i++;
        goto top;
    } else {
        m(i);
    }
bottom:
    n(i);












