''' Goal:  Become a black belt with properties.

What do properties do?
* Transform attribute access a.x into method access like a.m()

What is attribute access normally like:
* a.radius               -> finds "radius" in the instance dict
* a.version              -> find "version" in the class dict
* a.kind                 -> find "kind" in the parent class dict
* a.unknown              -> raises AttributeError

Computed fields using properties:
* Save space
* Reduce/Eliminate risk of data inconsistency
* Provide a consistent API for the user

Anti-pattern using setters on computed fields:

* It is okay to use a setter when action is unambigous:
    c.radius = 10     has an unambigous effect on the diameter
    
* It is not okay to use a setter when the action is ambiguous:
    invoice.total *= 2      should price change or the quantity
    p.midpoint = 15         should the low or high change and by how much
    
* The solution is to use a clearly named method to remove the ambiguity:
    invoice.change_total_by_altering_quantity(invoice.totat * 2.0)
    p.recenter_with_constant_range(15)

Managed attributes using properties:
* Control all reads and writes to an attribute
* They allow data validation at the moment data is stored
* This allows timely detection of data corruption
* Which speeds debug enormously

It is common to have a separate module of data validators:
* validate_percentage
* validate_string(maxlen=100)
* validate_one_of('english', 'metric')

'''

from __future__ import division
from validators import validate_percentage

class PriceRange(object):        # new-style classes inherit from object so that property will work.

    def __init__(self, low, high):
        self.low = low
        self.high = high

    @property                    # midpoint = property(midpoint)
    def midpoint(self):
        return (self.low + self.high) / 2

    def recenter_with_constant_range(self, newmid):
        'Recenter around new midpoint, keeping the range constant'
        half = self.high - self.midpoint
        self.low = newmid - half
        self.high = newmid + half

    # Managed attribute:  all reads and writes are controlled by code

    @property
    def low(self):
        'Managed attribute for "low" that validates percentages'
        return self._low

    @low.setter
    def low(self, low):
        validate_percentage(low)
        self._low = low

    @property
    def high(self):
        'Managed attribute for "high" that validates percentages'
        return self._high

    @high.setter
    def high(self, high):
        validate_percentage(high)
        self._high = high

p = PriceRange(10, 18)










