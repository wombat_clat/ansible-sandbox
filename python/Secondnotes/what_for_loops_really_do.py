'''

# ITERATOR Protocol

# ITERABLE -- Anything that can looped over -- Anything usable with a for-loop
#          -- It may or may not have mutable state.

# ITERATOR -- Usually refers to an iterable, but also has state
#          -- state is where we are during iteration
# 1. Fetch and return a new value
# 2. Update the state
# 3. Signal when the looping is done.

for (i=0 ; i<10 ; i++) {
    printf("%d\n", i);
    i = 5;
}

0
6
6
6
6
6
6
'''

for i in range(10):
    print i
    i = 5

it = iter(range(10))
while True:
    try:
        i = next(it)
    except StopIteration:
        break
    print i
    i = 5
