''' Learn the terminology, notation, way of thinking, and use patterns for decorators.

Identity function:   The output is always the same as the input.
Higher-order functions:  Use other functions as an input or as an output.
Pure function:  Gives the same output every time for the same input
                and has no side-effects on external state.

Functions are first-class objects.
Instance data:
    f.__class__
    f.__name__            <== Name given at birth
    f.__doc__             <== Default to None
    f.__code__.co_code
    f.__closure__         <== Remember data from a nested scope
Methods:
    f.__call__()
    f.__repr__()


Assigning words in Python:  =   import   class   def
All assignments in a function to into locals by default
Chain of namespace lookups:
    locals() -> nested_scope -> globals() -> __builtins__ -> NameError
               (closure vars)

Closure:  an inner function remembers (traps or closes)
   the variables from the enclosing namespace to make
   sure they are available later when needed.
 
When do you want a def-in-a-def?
   Use it when you want to create many functions
   whose code is same, but whose closed variables differ.

Wrapper or decorator:
    Add functionality to an existing object, class, or function.
    Technically, it is a wrapper is you have to go through the
    wrapping paper to get to the original; otherwise, it is
    called a decorator.
    
* One use of a wrapper is reduce arity by freezing some of the arguments
* One use of a wrapper is improve error handling
* One use of a wrapper is to add debugging info
* One use of a wrapper is adding robustness to existing tools

Monkey patching:
* Altering a function, variable, or class in someone else's namespace
* It is typically used to modify code you don't control

'''

from functools import wraps

def identity(x):
    'An higher-order, pure, identity function'
    return x

dispatch = {}

def register(func):
    'A higher-order, impure, identity function that register functions by name'
    name = func.__name__
    dispatch[name] = func
    return func

def add_logging(func):
    'Factory function to create function wrappers with logging'
    @wraps(func)
    def inner(x):
        print 'The', func.__name__, 'function was called with', x
        answer = func(x)
        print 'The answer is', answer
        return answer
    return inner

def cache(func):
    'Wrap a function with caching behavior'
    answers = {}
    @wraps(func)
    def inner(x):
        if x in answers:
            return answers[x]
        answer = func(x)
        answers[x] = answer
        return answer
    return inner

def execute(x = 10):
    '''How to create a simple DSL (domain specific language)

        >>> execute(10)
        Enter a series of transformations on 10: square square collatz big_calc collatz collatz
          square -> 100
          square -> 10000
         collatz -> 5000
        Doing hard work
        big_calc -> 5001
         collatz -> 15004
         collatz -> 7502

    '''
    program = raw_input('Enter a series of transformations on 10: ').split()
    for command in program:
        x = dispatch[command](x)
        print '%8s -> %s' % (command, x)


if __name__ == '__main__':

    ## Sample data (functions) ##########################################

    import time

    @cache
    @add_logging
    @register                     # square = register(square)
    @identity                     # square = identity(square)
    def square(x):
        'Return a value times itself'
        return x * x

    @cache
    @add_logging
    @register
    @identity
    @identity
    def collatz(x):
        if x % 2 == 0:
            return x // 2
        return 3 * x + 1

    @cache
    @add_logging
    @register
    def big_calc(x):
        'Simulate an expensive calcuation'
        print 'Doing hard work'
        time.sleep(1)
        return x + 1

