'Collection of data validation utilities'

def validate_percentage(value):
    'Verify a value is an int or float between 0 and 100'
    if not isinstance(value, (int, float)):
        raise TypeError('Expected an int or float')
    if value < 0.0 or value > 100.0:
        raise ValueError('Expected value between 0 and 100')
