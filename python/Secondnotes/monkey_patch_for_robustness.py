''' Legitimate use case for monkey patching:

        Make underlying functions more robust

We have code that depends on algebra.py that we don't control.

A function is more robust when it handles a broader range of inputs.

'''
## Monkey patch math to handle negative square roots ##

import math

orig_sqrt = math.sqrt   # keep the original which will do the actual work

def better_sqrt(x):
    'Wrapper to help square root support negative inputs'
    if x >= 0.0:
        return orig_sqrt(x)
    return orig_sqrt(-x) * 1j

math.sqrt = better_sqrt       # monkey patch

#######################################################

import algebra

a, b, c = 15, 19, 6
x1, x2 = algebra.quadratic(a, b, c)
print 'The roots for', a, b, c, 'are', x1, 'and', x2

a, b, c = 15, 19, 25
x1, x2 = algebra.quadratic(a, b, c)
print 'The roots for', a, b, c, 'are', x1, 'and', x2
