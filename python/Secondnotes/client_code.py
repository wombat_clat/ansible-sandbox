from __future__ import division
from circuitous import Circle

c = Circle(10)
print 'Tutorial for Circuitous(tm)'
print 'Version %d.%d' % Circle.version[:2]
print 'A circle of radius', c.radius
print 'has an area of', c.area()
print

## Academia -- Grant Proposal ###############################

from random import seed, random
from pprint import pprint

print 'DARPA Grant Proposal'
print 'To study a new mathematical constant:'
print 'The average area of random circles'
n = 10000
print 'Selecting %d random circles' % n
print "seeded with Jenny's number"
seed(8675309)
circles = [Circle(random()) for i in xrange(n)]
areas = [c.area() for c in circles]
average_area = sum(areas) / n
print 'The average area is %.5f' % average_area
print

## Rubber Sheet Company ###############################

template = [0.1, 0.2, 0.7]
print 'Specifications for the cut template:', template
circles = [Circle(radius) for radius in template]
for c in circles:
    print 'A cut of radius', c.radius
    print 'has a perimeter of', c.perimeter()
    print 'and a cold area of', c.area()
    c.radius *= 1.1
    # c.set_radius(c.get_radius * 1.1)  technical imperative and cultural norm in C++ and Java, but not Python
    print 'and a warm area of', c.area()
    print

## National Tire Company #############################

class Tire(Circle):
    'Tires has circle attributes but the circumference is corrected for the rubber on the tire'
    def perimeter(self):
        return Circle.perimeter(self) * 1.25           # Extending:   The parent is called and we modify the result.
        # return 2.0 * math.pi * self.radius * 1.25    # Overriding:  The parent method never gets called (risks DRY violation)

    __perimeter = perimeter
                
t = Tire(25)
print 'A tire with an inner radius of', t.radius
print 'has an inner area of', t.area()
print 'and a rubber corrected circumference of', t.perimeter()
print

# lookup order: t.a:  inst -> class -> parent -> AttributeError
# t.radius             rad
# t.perimeter          X      perim     ....
# t.area               X       X        area
#                        <--------------- |
# t.perimeter          ->>     perim    ....

## National Trucking Company #############################

print u'An inclinometer reading of 5\N{degree sign}'
print 'is a %.1f%% grade' % Circle.angle_to_grade(5)
print

## National Graphics Company ############################

c = Circle.from_bbd(25)
print 'A circle with bounding box diagonal of 25'
print 'has a radius of', c.radius
print 'and an area of', c.area()
print 'and perimeter of', c.perimeter()
print

## U.S. Government #####################################

# ISO 10666:  No circle software shall compute an area
# directly from a radius.  It MUST call perimeter and
# infer the radius indirectly.

# ISO 10667:  All circle software MUST store the diameter
# and ONLY the diameter.  It SHALL NOT store the radius.
