u'Circuitous\u2122 -- An Advanced Circle Analytics Company'
# Name of the project -- Intellectual Property -- Elevator Pitch

import math                             # Modules are for code re-use.
from collections import namedtuple      # Self-documenting tuples -- Easy way to improve code clarity

Version = namedtuple('Version', ['major', 'minor', 'micro'])

class Circle(object):              # New-style class inherit from object to gain new abilities.
    'An advanced circle analytics toolkit'

    __slots__ = ['diameter']       # Implement the Flyweight Design Pattern to save memory by suppressing the instance dictionary

    version = Version(0, 9, 1)     # Class variables are where you store information SHARED by all instances

    def __init__(self, radius):
        self.radius = radius       # Instance variables are where you store information UNIQUE to each instance.

    def area(self):                # Regular method:  "self" as the first argument
        'Perform quadrature on a planar shape of uniform revolution'
        p = self.__perimeter()     # Class local references are used when you need "self" to really be you
        radius = p / 2.0 / math.pi
        return math.pi * radius ** 2.0

    def perimeter(self):
        'Compute the closed line integral for a 2-d locus of points equidistant from a given point'
        return 2.0 * math.pi * self.radius

    __perimeter = perimeter       # Double leading underscores name mangle to _Circle__perimeter to implement class local reference for Open/Closed Principle

    def __repr__(self):           # "self" is a misnomer.  "self" really means you OR your children
        return '%s(%r)' % (self.__class__.__name__, self.radius)

    @staticmethod                 # static methods reprogram the dot to not add "self"
    def angle_to_grade(angle):    # use case:  adding normal functions that don't need "self" to classes to improve findability
        'Convert an inclinometer reading in degrees to a percent grade'
        return math.tan(math.radians(angle)) * 100.0

    @classmethod                  # class methods reprogram the dot to add "cls" instead of "self"
    def from_bbd(cls, bbd):       # use case:  creating alternative constructors so that everyone wins.
        'Create a new circle from a bounding box diagonal'
        radius = bbd / math.sqrt(2.0) / 2.0
        return cls(radius)

    @property                     # Reprogram the dot to change attribute access into method access automatically
    def radius(self):
        'Computed field using the diameter'
        return self.diameter / 2.0

    @radius.setter
    def radius(self, radius):
        self.diameter = radius * 2.0
