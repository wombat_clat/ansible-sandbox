def f(thing):
    if isinstance(thing, collections.Iterable):
      for x in thing:
          ...
    else:
      do_something_else(thing)


def grep(f, word):
    if isinstance(f, file):
        s = f.read()
    elif isinstance(f, str):
        with open(f) as g:
            s = f.read()
    else:
        raise ValueError('Expected either a file or string')
    return re.findall(word, s)

# Multiple dispatch

def grep(f: file, word):
    s = f.read()
    return re.findall(word, s)

def grep(f: str, word):
    with open(f) as g:
        s = f.read()
    return re.findall(word, s)

grep(string_representing_a_filename_or_a_file_open_for_reading, word:str) -> list of strings
# signature is crazy

# advice write two function

def _grep_str(s, word):
    return re.findall(word, s)

def grep_file(fileobj, word):
    s = fileobj.read()
    return _grep_str(s, word)

def grep_filename(filename, word):
    with open(fileobj) as g:
        s = f.read()
    return _grep_str(s, word)

print grep_filename('notes/hamlet.txt', 'polonius')

    
