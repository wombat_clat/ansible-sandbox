''' Goal:  CREATE a REST API server using itty

Mant Other MicroWeb Frameworks
  itty bottle flask cherrypy ...

All feature a register decorator to associate a route/path with a function

somewebsite.com/funstuff/thing_to_do?q=jan
|--- domain --||------ path --------||-- query --|
{ path1: func1,  path2: func2 }

Create functions that can be accessed by:  browser, curl, python, perl, java, ...
'''

from itty import *
import time, json, os, subprocess, re
from circuitous import Circle

@get('/')
def welcome(request):
    return 'Howdy!'

@get('/now')
def get_current_time(request):
    return Response(time.ctime(), content_type='text/plain')

@get('/notes')
def show_notes(request):
    files = os.listdir('notes')
    resp = json.dumps(files, indent=2)
    return Response(resp, content_type='application/json')

@get('/stats')
def show_netstat(request):
    resp = subprocess.check_output('netstat -a', shell=True)
    return Response(resp, content_type='text/plain')

@get('/upper/(?P<word>[A-Za-z]+)')
def upper_case_service(request, word):
    # localhost:8000/upper/raymond
    resp = word.upper()
    return Response(resp, content_type='text/plain')

@get('/circle/area')
def area_service(request):
    # localhost:8000/circle/area?radius=10 
    query = request.GET                               # query = {'radius': '10'}
    radius = float(query.get('radius', '0'))
    c = Circle(radius)
    a = c.area()
    resp = json.dumps(a, indent=2)
    return Response(resp, content_type='application/json')    

if __name__ == '__main__':
    run_itty(host='localhost', port=8000)
