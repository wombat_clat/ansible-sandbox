''' Legitimate use case for monkey patching:
        Improve the error message in code you don't control
        (in this case, the math module)
'''

import math

class InvalidSinInput(ValueError):
    'New exception for clarity'
    pass

original_asin = math.asin

def better_arc_sine(x):
    'Wrapper to improve the error message for asin()'
    try:
        return original_asin(x)
    except ValueError:
        raise InvalidSinInput('%r is an invalid input for sin()' % x)

	
math.asin = better_arc_sine           # The actual monkey patch

if __name__ == '__main__':
    # Demonstrate that the monkey patch works on user code
    
    import math
    math.asin(50)
