''' Simulate third-party code (an external package) we don't control

    But it has an design error that limits its usefulness.
    
'''

import math

def quadratic(a, b, c):
    'Return the two real roots of ax^2 + bx + c = 0'
    discrim = math.sqrt(b**2.0 - 4.0 * a * c)
    x1 = (-b + discrim ) / (2.0 * a)
    x2 = (-b - discrim ) / (2.0 * a)
    return x1, x2


