#! /usr/bin/env python
''' Create a command-line word frequency counter.

    Make it executable:
        $ chmod +x freq.py

    Make it findable from anywhere:

        $ alias freq='/Users/raymond/sj/freq.py'
    or:
        $ ln -s /Users/raymond/sj/freq.py /usr/local/bin/freq

'''

import collections
import re

def popular(text, limit=10, pattern=r"[a-z'\-]{2,}"):
    'Return the most popular words in the text'
    words = re.findall(pattern, text.lower())
    return collections.Counter(words).most_common(limit)

if __name__ == '__main__':

    import sys
    
    if len(sys.argv) <= 1:
        print >> sys.stderr, "Usage:  freq [filename] [limit]"
        sys.exit(1)
    filename = sys.argv[1]

    limit = 30
    if len(sys.argv) == 3:
        limit = int(sys.argv[2])

    with open(filename) as f:
        text = f.read()
    word_counts = popular(text, limit)
    print 'The %s most common words (more than 3 letters):' % limit
    for i, (word, count) in enumerate(word_counts, 1):
        print '%2d: %5d %s' % (i, count, word)
