'''Practical use case for creating your own content managers

When you see the same setup and teardown logic repeated,
you need a content manager to clean-up your code.

'''

import dis
import sys

class Redirect(object):

    def __init__(self, target):
        self.target = target

    def __enter__(self):
        self.oldstdout = sys.stdout
        sys.stdout = self.target

    def __exit__(self, exctype, excinst, exctb):
        sys.stdout = self.oldstdout
        

print 'Hello'
print 'Goodbye'

##oldstdout = sys.stdout
##sys.stdout = sys.stderr
##try:
##    help(bin)
##finally:
##    sys.stdout = oldstdout

with Redirect(sys.stderr):
    help(bin)

def square(x):
    return x * x

##with open('square.txt', 'w') as f:
##    try:
##        oldstdout = sys.stdout
##        sys.stdout = f
##        dis.dis(square)
##    finally:
##        sys.stdout = oldstdout

with open('square.txt', 'w') as f:
    with Redirect(f):
        dis.dis(square)


print 'Now we are done'
