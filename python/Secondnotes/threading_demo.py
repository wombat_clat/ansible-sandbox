''' Learn fear and respect and ablity with multi-threaded code.

RR1:  Get your application debugged in a single threaded mode
      before layering in multi-threading.

RR2:  Testing cannot prove the absence of errors.
      It is still useful, but don't count on it
      to reveal race conditions (many of these
      won't manifest themselves).

RS10:
    97.3% of threading problems are race conditions
    or scheduling problems.  Good news:  These are
    reliably solvable.  Bad news:  The remaining 2.7%
    of problems are hard.

RS11:
    Locks don't lock anything.  They are just flags and
    be ignored.  It is a cooperative tool, not an
    enforced tool.

RFC1000:
    All shared resources SHALL be run in exactly ONE thread.
    All communication with the thread SHALL be done using
    atomic messaging, typically the Queue module or message
    queues like (zeromq, rabbitmq, etc).

    Resources:  global variables, user input, output devices,
    files, sockets, databases, email, and logging.

    We don't worry about:  databases, logging, email
    because they already have their own locking mechanisms.

RFC1001:
    Within a thread, everything runs sequentially.
    Coordination between threads SHALL be sequenced by joining:
    
    1) If a thread can return (non-daemon), use t.join()
    which waits for the thread to return.

    2) If thread never returns (daemon), use q.join()
    which waits to the queue to be notifies that tasks
    have been BOTH gotten AND completed.

GIL -- Global Interpreter Lock
    -- Specific to CPython (not PyPy, not Jython, not IronPython)
    -- Rule you can run as many threads you want, BUT
       only one thread can run at a time.  Zero benefit from multicore.
       
    Threading problems fall into 2 categories:
    * I/O Bound (waiting on sockets, urls, SDD, HD, database, email).
      Threads work great for these.
    * CPU Bound (100% cpu utilization)
      Threads are a disaster for these.
      This will be SLOWER than single-threaded because
      you've aded the thread-switching overhead.

    If you need multicore advantage for CPU bound:
    * use processes instead (no shared state and no GIL)
    * use Jython, IronPython, and to lesser extent PyPy
      which has a GIL but it has much less global state
      so it doesn't block as much.

      python hello.py         # Cpython
      jython hello.py         # Java version
      ironpython hello.py     # Dot Net version
      pypy hello.py           # Python in Python (JIT)
'''

import Queue
import threading

## Isolate the shared global variable resource ##################

counter = 0

count_queue = Queue.Queue()

def counter_manager():
    'I have EXCLUSIVE rights to update the counter'
    global counter
    while True:
        increment = count_queue.get()        # blocks until a message is received.
        # Everything from here runs sequentially AND no one else is in this race.
        counter += increment
        print_queue.put([
            'The count is',
            str(counter)
        ])
        count_queue.task_done()

t = threading.Thread(target=counter_manager)
t.daemon = True          # For threads that never return, don't keep the main thread from exiting.
t.start()
del t

## Isolate the shared printer resource #####################

print_queue = Queue.Queue()

def print_manager():
    'I have EXCLUSIVE rights to use the "print" keyword'
    while True:
        job = print_queue.get()        # blocks until a message is received.
        # Everything from here runs sequentially AND no one else is in this race.
        for line in job:
            print line
        print_queue.task_done()

t = threading.Thread(target=print_manager)
t.daemon = True          # For threads that never return, don't keep the main thread from exiting.
t.start()
del t

#################
def worker():
    'My job is to update the counter and print the current count'
    count_queue.put(1)

print_queue.put(['Starting up'])

worker_threads = []
for i in range(10):
    t = threading.Thread(target=worker)
    t.start()
    worker_threads.append(t)
# At this point ten workers are running in parallel

# Now let's make they all get a chance to finish
for t in worker_threads:
    t.join()                   # blocks until the worker thread has returns.
# We are guaranteed that the worker threads have all finished.

count_queue.join()           # blocks until all counter increment request have been completed
# We are guaranteed that ten increments have happens and ten print job were submitted.
    
print_queue.put(['Finishing up'])

# At this point, we know 12 print jobs have been submitted to the print_queue.
print_queue.join()            # block until all print jobs have finished printing.

# We are guaranteed the 12 print jobs have finished printing
# So now it is okay for the main thread to exit
# In the interpreter, the >>> will now print
# At the command line, we exit back to bash
