''' Goal:  learn how context managers work

with a as b:    b = a.__enter__()    withable         ->    context manager
    ...         a.__exit__()

'''

import sys

# The Old Way to open and close a file

f = open('notes/hamlet.txt')
try:
    play = f.read()
    print len(play)
finally:
    f.close()

# The New Way to open and close a file

with open('notes/hamlet.txt') as f:
    play = f.read()
    print len(play)    

# How the new way works -- The inner details
_t = open('notes/hamlet.txt')
f = _t.__enter__()
exc = None
try:
    play = f.read()
    print len(play)
except Exception as e:
    exc = e
finally:
    if exc is None:
        _t.__exit__(None, None, None)
    else:
        if not _t.__exit__(type(exc), exc, sys.traceback(exc)):
            raise

# How to make a lock #################################
#                Mutex:  Assures that two thread don't
#                simulatenously need a resource

import threading

print_lock = threading.Lock()

# How to use a lock -- The Old Way(tm) ###############

print_lock.acquire()         # This blocks until the resource becomes available
try:
    print 'Critical section 1'
    print 'Critical section 2'
finally:
    print_lock.release()

# How to use a lock -- The New Way(tm) ###############

with print_lock:
    print 'Critical section 1'
    print 'Critical section 2'
