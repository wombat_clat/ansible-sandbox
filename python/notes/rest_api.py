''' Demonstrate how to create REST APIs
    using itty which is a tiny version
    of Bottle or Flask.

'''

from notes.itty import *
import time, json, os, subprocess
from class_demo import Circle

@get('/')
def welcome(request):
    return 'Howdy!'

@get('/now')
def what_time_is_it(request):
    return time.ctime()

@get('/files')
def show_files(request):
    files = os.listdir('notes')
    response = json.dumps(files, indent=4)
    return Response(response, content_type='application/json')

@get('/stats')
def show_disk_stats(request):
    response = subprocess.check_output('df', shell=True)  # chkdsk for Windows    
    return Response(response, content_type='text/plain')

# curl http://localhost:8080/circle?radius=10&q=area
# curl http://localhost:8080/circle?radius=10&q=circum

@get('/circle')
def circle_operations(request):
    params = request.GET
    radius = int(params.get('radius', '0'))
    action = params.get('q', '')
    c = Circle(radius)
    if action == 'area':
        result = c.area()
    elif action == 'circum':
        result = c.perimeter()
    else:
        raise NotFound('unknown action')
    response = json.dumps(result)
    return Response(response, content_type='application/json')
    

if __name__ == '__main__':

    run_itty(host='', port=8080)

