import unicodedata

# This part is straight unicode ##########################

s = 'L' + unichr(111) + unichr(776) + 'wis'
s += ' ' + unichr(1083) + unichr(1091) + unichr(1095)
print s
print type(s)
print map(ord, s)

# Normalize the combining characters
t = unicodedata.normalize('NFC', s)
print t
print map(ord, t)

for c in t:
    print c, ord(c), unicodedata.name(c), unicodedata.category(c)

## Encodings -- store as array of bytes ########################

e = t.encode('UTF-8')
print map(ord, e)       # none of the ords will be bigger than 255
print e.decode('UTF-8')

e = t.encode('UTF-16')
print map(ord, e)       # none of the ords will be bigger than 255
print e.decode('UTF-16')

e = t.encode('UTF-32')
print map(ord, e)       # none of the ords will be bigger than 255
print e.decode('UTF-32')

# Techniques for getting ASCII out of unicode
print t.encode('ascii', 'ignore')       # Drop invalid characters
print t.encode('ascii', 'replace')      # Convert invalid to ?

# Technique (a bit of a hack)
# used to transport binary data in JSON (utf-8)
binary_data = ''.join(map(chr, [3, 14, 15, 9, 26, 5, 35]))
unicode_data = binary_data.decode('latin-1')
original_data = map(ord, unicode_data.encode('latin-1'))
