''' Teach everything you need to know about function decorators!

In Python, functions are first-class objects:

    * You can create new ones at a run-time
    * You can save them in data structures
    * You can pass them into return them from other functions

Function in Python is an object of a class, just like everything else in the language:
    Instance data:
        f.__class__
        f.__name__
        f.__doc__
        f.__code__.co_code
    Class:
        function.__call__

Pure function:  same answer for same input every time without side-effects
Identity function:  the output is the exact same as the input
Higher-order function:  takes another function as input or returns a function as output

A decorator or wrapper keeps the underlying function but adds a new
capability such as debugging, registration, robustness, error handling.

'''

from functools import wraps

registry = set()

dispatch = {}

def identity(func):
    'Pure, higher order, identity function'
    return func

def register(func):
    'Keep a list of function names in a registry list'
    name = func.__name__
    registry.add(name)
    return func

def assign(name):
    '''Makes a decorator to assign a particular name

        @assign('box')
        def square(x):
            return x * x

        deco = assign('box')

        @deco
        def square(x):
            return x * x

        dispatch = {'box': <square func>}

    '''
    def deco(func):
        'Build a dispatch dictionary mapping a function name to a function'
        # Useful for name based dispatching.  See dispatcher.py
        dispatch[name] = func
        return func
    return deco

def add_logging(origfunc):
    'Wrap a function to add call logging for debugging purposes'
    @wraps(origfunc)
    def newfunc(*args):
        print origfunc.__name__, 'was called with', args
        answer = origfunc(*args)
        print 'The answer is', answer
        return answer
    return newfunc

def cache(origfunc):
    answers = {}
    @wraps(origfunc)
    def newfunc(*args):
        if args in answers:
            return answers[args]
        answer = answers[args] = origfunc(*args)
        return answer
    newfunc.__doc__ = 'Cached function: ' + newfunc.__doc__
    return newfunc

### Test data for higher-order functions ########################
### Functions are just data #####################################

if __name__ == '__main__':

    import time

    @assign('pointy')
    @add_logging
    @assign
    @register              # square = register(square)
    @cache
    def square(x):
        'Return a value times itself'
        return x * x

    @assign('test_collatz')
    @register
    @register
    def collatz(x):
        if x % 2 == 0:
            return x // 2
        return 3 * x + 1

    @assign('slow')
    @cache
    def big_calc(x):
        'Simulate a big calc'
        print 'Doing hard work!'
        time.sleep(1)
        return x + 1
