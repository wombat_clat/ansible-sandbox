/* structured control-flow  RATFOR */

for (i=0 ; i<n ; i=i+1) {
    f(i);
    if (k(i)) {
        m(i);
        break;
    }
    if (g(i)) {
        h(i);
        continue;
    }
    j(i);
} else {              /* runs when the loop condition fails */
    n(i);
}
q(i);




i = 0;
while (i<n) {
    f(i);
    if (k(i)) {
        m(i);
        break;
    }
    if (g(i)) {
            h(i);
            i = i + 1;
            continue;
    }
    j(i);
    i = i + 1;
} else {
    n(i);
}
q(i);



i = 0;
top:
    if (i<n) {
        f(i);
        if (k(i)) {
            m(i);
            goto done;
        }
        if g(i) {
            h(i);
            i = i + 1;
            goto top;
        }
        j(i);
        i = i + 1
        goto top;
    } else {
        n(i);
    }
done:
    q(i);
