''' Compute the famous Fibonacci function:

        F(0) -> 0,
        F(1) -> 1,
        F(n) -> F(n-1) + F(n-2),  n >= 2

Trace the performance with:

    $ python -m trace --count fibo.py

'''

from decorator_school import cache

def fibo(n):
    'Compute the Fibonacci function'
    a, b = 0, 1
    for i in xrange(n):
        a, b = b, a + b
    return a

@cache
def fibo(n):
    'Compute the Fibonacci function'
    if n == 0: return 0
    if n == 1: return 1
    return fibo(n-1) + fibo(n-2)

if __name__ == '__main__':

    for n in range(100):
        print n, '<--', fibo(n)
