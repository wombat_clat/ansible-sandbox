from math import pi

class Circle(object): 

    def __init__(self, radius):
        self.radius = radius 

    def area(self):
        print 'Internal:', self.__perim()
        return pi * self.radius ** 2.0

    def perimeter(self):
        return 2.0 * pi * self.radius

    __perim = perimeter

class Tire(Circle):

    def perimeter(self):
        return Circle.perimeter(self) * 1.25   # Extending the parent

    __perim = perimeter

t = Tire(25)
print 'A tire with an inner radius of', t.radius
print 'has an inner area of', t.area()
print 'and outer circumference of', t.perimeter()
print

# t    ->  Tire -> Circle
# inst -> class -> parent -> AttributeError
# radius
#         perim     perim
#                   area
#                             color
#         _Tire__perim
#                _Circle__perim
