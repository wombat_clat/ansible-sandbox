'Show how to dispatch functions by name'

from decorator_school import dispatch

commands = raw_input('Type in your commands: ')

x = 5
for command in commands.split():
    f = dispatch[command]
    x = f(x)
    print x
