''' Show how to use monkey patching
    to add debugging capability to
    code you don't control (math and class_demo)

    Monkey patching lets you fix the dependent module
    so that the other module runs correctly
    or add debugging capability or adds robustness.

Goal:  make the tan calculation visible for debugging
'''

import math

origtan = math.tan

def logging_tan(x):
    'Wrap tan() to add logging capability'
    print 'Tan() called with', x
    answer = origtan(x)
    print 'The answer is', answer
    return answer

# The actual monkey patch
math.tan = logging_tan

# Observe the effect on the class_demo

from class_demo import Circle
g = Circle.angle_to_grade(5)
