'High-school algebra'

import math

def quadratic(a, b, c):
    'Find roots of a*x**2 + b*x + c == 0'
    discriminant = math.sqrt(b**2 - 4 * a * c)
    x1 = (-b + discriminant) / (2 * a)
    x2 = (-b - discriminant) / (2 * a)
    return x1, x2

if __name__ == '__main__':

    # (3x + 2) (5x - 3)
    # 15x**2  + 1 x - 6
    print quadratic(15, 1, -6)
    # print quadratic(15, 1, 6)
