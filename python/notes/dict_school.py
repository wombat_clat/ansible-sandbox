d = dict(raymond='mac', rachel='pc', matthew='vtech')
 
print d['raymond']                               # []
print d.__getitem__('raymond')

d['raymond'] = 'mac air'                         # [] =
d.__setitem__('raymond', 'mac air')
print d

del d['raymond']                                 # del []
d.__delitem__('rachel')

d = dict(raymond='mac', rachel='pc', matthew='vtech')
print len(d)
print d.__len__()

# The square brackets lookups are CONDITIONAL, the can fail with a KeyError if a key is missing

print d.get('raymond', 'slide rule')
print d.get('roger', 'slide rule')
print d

# The dict.get() method is UNCONDITIONAL and always succeeds by at least returning the default
# The dict.get() methods has NO SIDE-EFFECTS (dict is unchanged)

print d.setdefault('raymond', 'slide rule')
print d.setdefault('roger', 'slide rule')
print d

# The dict.setdefault() method is UNCONDITIONAL and always succeeds by at least returning the default
# It returns EXACTLY the same as dict.get()
# The dict.setdefault() method has SIDE-EFFECT:  missing keys get added with the default

print d.pop('raymond', 'slide rule')
print d.pop('john', 'slide rule')
print d

# The dict.pop() method is UNCONDITIONAL and always succeeds by at least returning the default
# It returns EXACTLY the same as dict.get()
# The dict.pop() method has SIDE-EFFECT:  existing keys get removes

e = d                # No copy.   D and E are shared
d.clear()            # The one shared dict is emptied
print d
print e

d = dict(raymond='mac', rachel='pc', matthew='vtech')
e = d.copy()         # Real copy.  D and E are different
d.clear()            # D is emptied
print d
print e              # E still has data

d = dict(raymond='mac air', john='slide rule')
print d
print e
e.update(d)          # copies and overwrites from d
print e

# The entries in a dict display in arbitrary order
# however, the keys/values/items are guaranteed to correspond
print d.keys()
print d.values()
print d.items()
print 'Guaranteed:',  zip(d.keys(), d.values()) == d.items()

# List output is inefficient for big dictionaries
# Iterators are better
for k in d:
    print 'Key:', k
for k in d.iterkeys():        # This is equivalent and unnecessary
    print 'Key:', k
for v in d.itervalues():
    print 'Value:', v
for k, v in d.iteritems():
    print 'Key: %r  ->  Value: %r' % (k, v)

# Return and remove and arbitrary key/value pair

d_popitem = d.popitem
try:
    while 1:
        # this is atomic, so it get used in multithreading w/o locks
        print 'Key: %r  ->  Value: %r' % d_popitem() 
except KeyError:
    pass

####################################################################

# d[k] -> v
# d.__getitem__(k)
#    Looks up k in the hash table. and if found returns the corresponding v
#    If k is not found, calls d.__missing__(k)
# d.__missing__(k)
#    Raises KeyError

####### Modifying lookup behavior #################################

#d = dict(raymond='mac', rachel='pc', matthew='vtech')
#print d['RAYMOND']

class CaseInsensitiveDict(dict):

    def __setitem__(self, key, value):
        key = key.lower()
        dict.__setitem__(self, key, value)
        
    def __getitem__(self, key):
        key = key.lower()
        return dict.__getitem__(self, key)

d = CaseInsensitiveDict()
d['RaYmOnD'] = 'red'
print d
print d['RAYMOND']

####### Modifying missing behavior while keeping lookup behavior ###################

class AngryDict(dict):
    'A dict that gets really mad when keys are missing'
    def __missing__(self, key):
        print 'I am SO angry.  The %r key is missing!' % key
        raise KeyError(key)

d = AngryDict(raymond='red', rachel='blue')

### Counting applications ###############################################

class ZeroDict(dict):
    def __missing__(self, key):
        return 0

colors = 'red green red blue green red'.split()
d = ZeroDict()
for color in colors:
    d[color] += 1
print d

from collections import Counter
d = Counter()
for color in colors:
    d[color] += 1
print d

from collections import defaultdict
d = defaultdict(int)
for color in colors:
    d[color] += 1
print d

class DefaultDict(dict):
    'A kind of dict that has a factory to create missing values'
    
    def __init__(self, default_factory, *args, **kwds):
        self.default_factory = default_factory
        dict.__init__(self, *args, **kwds)

    def __missing__(self, key):
        value = self.default_factory()
        self[key] = value
        return value
        
    def __repr__(self):
        return '%s(%r, %r)' % (self.__class__.__name__, self.default_factory, dict(self))

d = DefaultDict(int, raymond='red', rachel='blue')
print d['roger']
print d

d = DefaultDict(list, raymond='red', rachel='blue')
print d['roger']
print d

d = DefaultDict(int)
for color in colors:
    d[color] += 1
print d

###############################################
### Grouping #######################################################

from pprint import pprint
from itertools import imap

class ListDict(dict):
    def __missing__(self, key):
        value = self[key] = []
        return value

names = ''' raymond rachel matthew roger betty martin beatrice toby
sheldon tommy shelly  robert mary randy bess tyler '''.split()

print 'Group names by the first letter of the name'
d = ListDict()
for name in names:
    key = name[0]
    d[key].append(name)
pprint(d)

print 'Group names by the last letter of the name'
d = ListDict()
for name in names:
    key = name[-1]
    d[key].append(name)
pprint(d)

print 'Group names by the length of the name'
d = ListDict()
for name in names:
    key = len(name)
    d[key].append(name)
pprint(d)

print 'Group names by the number of vowels in the name:  aeiouy'
d = ListDict()
for name in names:
    key = name.count('a') + name.count('e') + name.count('i')
    key += name.count('o') + name.count('u') + name.count('y')
    # key = sum([name.count(v) for v in 'aeiouy'])
    # key = sum(imap(name.count, 'aeiouy'))
    d[key].append(name)
pprint(d)

d = defaultdict(list)  #  <== This does the same thing as our ListDict

### String Formatting #######################################

class DefaultFormatDict(dict):
    def __missing__(self, key):
        return '%(' + key + ')s'

d = DefaultFormatDict(new=10)
print 'The answer is %(new)s today but was %(old)s yesterday' % d
print 'The answer is %(new)s today but was %(old)s yesterday' % d % dict(old=20)

### Dict Chaining ############################################

class ChainDict(dict):
    
    def __init__(self, fallback, *args, **kwds):
        self.fallback = fallback
        dict.__init__(self, *args, **kwds)

    def __missing__(self, key):
        return self.fallback[key]

defaults = dict(fg='white', bg='black', width=80, height=25, border=False)
environ = ChainDict(defaults, width=70, height=20)
settings = ChainDict(environ, border=True)

print settings['fg'], '--> white'
print settings['width'], '--> 70'
print settings['border'], '--> True'

### Many things we like to do are effectively a chain of namespace lookups ######

# command line arguments beat environment vars which beat default settings
# commandline -> environ -> defaults

# current dir -> first in path -> next dir path -> ...

# locals -> globals -> builtins

# inst vars -> class dict -> parent class dict





