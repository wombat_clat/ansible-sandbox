''' Goal:  Become a black belt with property().

What does property() do?
* It transforms attribute access into method calls.

What is number one cause of it not working?
* Only works with new-style classes.

Computed fields using properties:
* Save space
* Assure data consistency
* Give API consistency
* The can have docstrings (like a data dictionary)

Setter property can be used to update attributes
but we don't like that for computed fields because
it is unclear what it doing (ambiguous).
Instead, don't use a property, use a method with
a VERY clear name and docstring.

Managed attributes use methods to control
ALL access to access to a variable:
* This is primary useful to validate data
  at the moment it is stored
* It is frequenty use during debugging to
  find data corruption at the point of
  corruption rather than downstream when
  the data is used.

Common module of validators:
* validate_percentage
* validate_one_of('win', 'lose', 'draw')
* validate_string(maxsize=100)
* validate_secure_connection

'''

from __future__ import division
from validators import validate_percentage
DEBUG = True

class PriceRange(object):

    def __init__(self, low, high):
        self.low = low
        self.high = high

    @property                    # midpoint = property(midpoint)
    def midpoint(self):
        'Computed field averaging high and low'
        return (self.low + self.high) / 2

    def move_midpoint_keeping_half_dist_constant(self, mid):
        half = self.high - self.midpoint
        self.low = mid - half
        self.high = mid + half

    def move_midpoint_keeping_low_constant(self, mid):
        half = mid - self.low
        self.high = mid + half

    if DEBUG:

        @property
        def low(self):
            return self._low

        @low.setter
        def low(self, low):
            validate_percentage(low)
            self._low = low

        @property
        def high(self):
            return self._high

        @high.setter
        def high(self, high):
            validate_percentage(high)
            self._high = high

p = PriceRange(10, 18)
p.move_midpoint_keeping_half_dist_constant(16)
p.move_midpoint_keeping_low_constant(16)

p = PriceRange(30, 36)

