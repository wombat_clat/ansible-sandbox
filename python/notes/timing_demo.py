''' Command-line scripts

~/sj $ python -m timeit -s 'a=10; b=20' 't=a; a=b; b=t'
10000000 loops, best of 3: 0.0273 usec per loop
~/sj $ python -m timeit -s 'a=10; b=20' 'a, b = b, a'
10000000 loops, best of 3: 0.0233 usec per loop
~/sj $ 
~/sj $ 
~/sj $ python -m timeit -s 'd={}' 'd[10]=20; d.popitem()'
10000000 loops, best of 3: 0.113 usec per loop
~/sj $ python -m timeit -s 'd={}; d_popitem=d.popitem' 'd[10]=20; d_popitem()'
10000000 loops, best of 3: 0.0807 usec per loop
~/sj $ python -m timeit -s 'd={}; d_popitem=d.popitem' 'd[10]=20'10000000 loops, best of 3: 0.0388 usec per loop

'''
