"Circuitous(tm) -- Advanced circle analytics company"
#  Name                Elevator Pitch

from math import pi, tan, radians, sqrt     # Module's a primary tool for code reuse
from collections import namedtuple

Version = namedtuple('Version', ['major', 'minor'])  # Self-describing tuple for readability

class Circle(object):              # New style class inherit behaviors from object
    'An advanced circle analytic toolkit'

    version = Version(0, 11)

    __slots__ = ['diameter']       # Implement the flyweight design pattern by suppressing the instance dictionary
    
    kind = 'roundish thingie'      # Class variable -- Store data that is SHARED by all the instances

    def __init__(self, radius):
        self.radius = radius       # Instance variable -- Store data that varies between instances

    def area(self):                # Regular method has "self" as the first argument
        'Perform quadrature on a planar shape of uniform revolution'
        p = self.__perimeter()     # Sometimes you want "self" to actually be you.  Class local reference.
        radius = p / 2.0 / pi
        return pi * radius ** 2.0

    def perimeter(self):
        'Compute the close line integral for a 2-D locus of points equidistant from a given point'
        return 2.0 * pi * self.radius

    __perimeter = perimeter        # Name mangling in support of class local references    

    def __repr__(self):
        return '%s(%r)' % (self.__class__.__name__, self.radius)

    @staticmethod                 # Reprograms the dot to not add "self" as the first argument
    def angle_to_grade(angle):    # Use case:  add regular functions to a class for better findability
        'Convert an inclinometer reading in degrees to a percentage grade'
        return tan(radians(angle)) * 100.0

    @classmethod                  # Reprograms the dot to add "cls" as the first argument
    def from_bbd(cls, bbd):       # Use case:  alternative constructors so that everyone wins
        'Alternative constructor creating Circles from a bounding box diagonal'
        radius = bbd / sqrt(2.0) / 2
        return cls(radius)

    @property       
    def radius(self):
        return self.diameter / 2.0

    @radius.setter
    def radius(self, radius):
        self.diameter = radius * 2.0

    def __getstate__(self):
        return self.radius

    def __setstate__(self, state):
        self.radius = state
                 
