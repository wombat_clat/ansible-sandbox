''' Master Threading and Gain Respect for It

97.3% of threading problems are either RACE CONDITIONS or SCHEDULING problems.
These problems are reliably solvable.

Solving RACE CONDITIONS:
------------------------
ALL shared resources SHALL BE isolated in EXACTLY ONE THREAD
with ONE THREAD per resource.   Resource include global variables,
user input, output, sockets, files.
ALL communication with that thread SHALL be done using a queue.

Solving scheduling problems (thing have to happen in a particular order)
------------------------------------------------------------------------
Anything series tasks that need to
be sequential SHOULD be put in a single thread.

Waiting for groups running in parallel to finish:
* Regular threads can join the thread with t.join()
* Daemon thread can join the queue wih q.join()

Hard problems (circular)
------------------------
2.7% of threading problems are hard.
We'll learn to recognize these.

'''

import threading
import Queue
import time

## Isolate a global variable ##############

counter = 0

counter_queue = Queue.Queue()

def counter_manager():
    'I have EXCLUSIVE rights to update the counter'
    global counter
    while True:
        increment = counter_queue.get()   # <-- This blocks
        counter += increment
        print_queue.put([
            'The count is',
            str(counter)
        ])
        counter_queue.task_done()
        
t = threading.Thread(target=counter_manager)
t.daemon = True
t.start()

## Isolate the printer resource ###########

print_queue = Queue.Queue()

def print_manager():
    'I have EXCLUSIVE rights to use the "print" keyword'
    while True:
        job = print_queue.get()
        for line in job:
            print line
        print_queue.task_done()

t = threading.Thread(target=print_manager)
t.daemon = True
t.start()

###########################################

def worker():
    counter_queue.put(1)        # <-- Wake up the counter_manager

print_queue.put(['Starting up'])

workers = []
for i in range(10):
    t = threading.Thread(target=worker)
    t.start()
    workers.append(t)

# Wait on all the worker threads to finish
for w in workers:
    w.join()             # <-- Wait on thread w to return

# Now we know all workers are done with their work
# which was to send email requesting an increment of one.

counter_queue.join()

# Now we know that all counter queue tasks have been gotten AND done.
# The task was update the count AND send a email requesting to print it.

print_queue.put(['Finishing up'])

print_queue.join()

# Now we know that all twelve print jobs are done.

    
