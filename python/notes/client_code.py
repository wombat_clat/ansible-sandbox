'Code from the users point of view'
# I fight for the user!

from __future__ import division
from random import random, seed

# Tutorial ##################################

from class_demo import Circle
print 'Demo of Circuitous(tm)'
print 'Version %d.%d' % Circle.version
c = Circle(10)
print 'A circle with a radius of', c.radius
print 'has an area of', c.area()
print

# Academia ##################################

n = 100000
seed(8675309)
print 'DARPA grant proposal:  Average area of average circles'
print 'Using Circuitous(tm) version %d.%d' % Circle.version
print 'Analysis of %d random circles' % n
print "Seeded using Jenny's number: 8675309"
circles = [Circle(random()) for i in xrange(n)]
areas = [c.area() for c in circles]              # areas = map(Circle.area, circles)
average_area = sum(areas) / n
print 'The average area is %.6f' % average_area
print

# Rubber Sheet Company ###############################
cut_template = [0.1, 0.7, 0.8]
circles = [Circle(radius) for radius in cut_template]
for c in circles:
    print 'A cut radius of', c.radius
    print 'gives a perimeter of', c.perimeter()
    print 'and a cold area of', c.area()
    c.radius *= 1.1
    print 'and a warm area of', c.area()
    print

# National Tire Chain ###############################

class Tire(Circle):
    'Tires are tracked by inner radius but specced of outer radius'
    def perimeter(self):
        'An odomoter corrected circumference that includes the rubber on the tire'
        return Circle.perimeter(self) * 1.25   # Extending the parent
    __perimeter = perimeter

t = Tire(25)
print 'A tire with an inner radius of', t.radius
print 'has an inner area of', t.area()
print 'and outer circumference of', t.perimeter()
print

# National Trucking Company ########################

print 'An inclinometer reading of 5 degrees'
print 'is a %.1f%% grade' % Circle.angle_to_grade(5)
print

# National Graphics Company #######################

c = Circle.from_bbd(25)
print 'A circle with bounding box diagonal of 25'
print 'has a radius of', c.radius
print 'an area of', c.area()
print 'and circumference of', c.perimeter()
print

## USA ########################################

# ISO 10666:  No circle software SHALL have an
# area() method that directly access instance data.
# Said software, SHALL call perimeter() and compute
# radius indirectly.

# ISO 10667:  No circle software SHALL store a
# radius.  It MUST store the diameter and ONLY
# the diameter.

## Data transport #############################

import pickle
c = Circle(12)
p = pickle.dumps(c)
d = pickle.loads(p)
print d
