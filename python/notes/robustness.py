''' Make the numeric module more ROBUST
    so that it can handle a broader range of inputs.

'''

import math

orig_sqrt = math.sqrt

def mysqrt(x):
    'Wrap sqrt to make it handle negative inputs'
    if x >= 0.0:
        return orig_sqrt(x)
    else:
        return orig_sqrt(-x) * 1j       
    
# Monkey patch the math module
math.sqrt = mysqrt

from numeric import quadratic

print quadratic(15, 1, -6)
print quadratic(15, 1, 6)
