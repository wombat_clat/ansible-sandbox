from collections import OrderedDict

cache_content = {
    '"8675-8912"': 'Hello World',
    '"1234-2345"': 'Goodbye Cruel World',
}

# Created by a remote requester
headers = '''Accept: */*
Accept-If: "1234-2345"
Content-Encoding: gzip
Cache-Control: None
'''

hd = OrderedDict()
for header in headers.splitlines():
    tag, value = header.split(':')
    hd[tag] = value

if hd['Cache-Control'] == 'None' and cached_content[hd.get('Accept-If')]:
    print 'Seen it before!'
    print cached_content[hd.get('Accept-If')]
    # early-out

else:
    hd['Via'] = 'IronPort'
    for tag, value in hd.items():
        print '%s: %s' % (tag, value)


    
    

