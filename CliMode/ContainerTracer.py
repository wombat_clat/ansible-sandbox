import CliMode


class ContainerTracerMode( CliMode.ConfigMode ):
    def enterCmd( self ):
        return 'containertracer'

    def __init__( self ):
        self.modeKey = 'containertracer'
        self.longModeKey = self.modeKey
        CliMode.ConfigMode.__init__( self, None )
