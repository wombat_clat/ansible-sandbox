import CliSave
import ContainerTracerLib
from IntfCliSave import IntfConfigMode
from CliMode.ContainerTracer import ContainerTracerMode
import base64


def _getEncryptedPasswordFor( self, clearTextPassword ):
    return base64.b64encode( clearTextPassword )


class ContainerTracerConfigSaveMode( ContainerTracerMode, CliSave.Mode ):
    def __init__( self, param ):
        ContainerTracerMode.__init__( self )
        CliSave.Mode.__init__( self, param )

CliSave.GlobalConfigMode.addCommandSequence( 'ContainerTracerConfig.global' )
CliSave.GlobalConfigMode.addChildMode( ContainerTracerConfigSaveMode,
                                       after=[ 'ContainerTracerConfig.global' ]
                                       )
ContainerTracerConfigSaveMode.addCommandSequence( 'ContainerTracerConfig.cfg' )


@CliSave.simpleSaver( '' )
def ContainerTracerConfigSave( entity, root, sysdbRoot, options ):
    ctl = ContainerTracerLib.ContainerTracer()
    mode = root[ ContainerTracerConfigSaveMode ].getOrCreateModeInstance(
        None )
    cmds = mode[ 'ContainerTracerConfig.cfg' ]
    # read json
    conf = ctl.read_config_file( )
    # eos_conf = ctl.read_eos_config_file( )

    cmds.addCommand( 'swarm ip %s port %s' % (conf[ 'SwarmConfig' ][ 'IP' ],
  	                                      conf[ 'SwarmConfig' ][ 'Port' ]
    	                                      ) )
    	# pswd = eos_conf.get( 'connection:eos', 'password' )
    cmds.addCommand( 'Eos username %s password %s %s host %s transport %s' %
    	              ( conf[ 'EosConfig' ][ 'Username' ],
    	               conf[ 'EosConfig' ][ 'Encryption' ],
    	               conf[ 'EosConfig' ][ 'Password' ],
    	               conf[ 'EosConfig' ][ 'Hostname' ],
    	               conf[ 'EosConfig' ][ 'Transport' ] ) )
