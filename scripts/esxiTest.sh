#!/bin/bash

HOST="panview"
ESXI="172.22.28.176"
USER="root"
PASS="arista123"
VAR=$(expect -c "
spawn ssh $USER@$ESXI
expect \"password:\"
send \"$PASS\r\"
expect \"\\\\$\"
send \"ls\r\"
expect -re \"$USER.*\"
send \"logout\"
")

echo "==============="
echo "$VAR"
