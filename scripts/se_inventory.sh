#!/bin/bash



USER="admin"
PASS="admin"
HOST="$1"

#Usage is as follows, [./script x.x.x.x ] 


inventory(){
         VAR=$(expect -c "
	 spawn ssh $USER@$HOST
         expect \"?\"
	 send \"yes\r\"
	 expect \"password:\"
         send \"$PASS\r\"
         expect \"\\\\$\"
	 send \"enable\r\"
	 expect \"\\\$\"
	 log_file /home/fernando/scripts/logs/r21.log;
	 send \"show version | egrep -i 'DCS|serial'\r\"
	 expect -re \"$USER.*\"
         log_file; 
         send \"logout\"
         ")
}

ping -c 1 "$HOST"
if [[ "$?" -eq "0" ]]
then
inventory
else
echo "$HOST not reachable" >> /home/fernando/scripts/logs/r22.log
exit 0
fi
