#!/bin/bash

cat ~/scripts/selabtor | while read servers
do
   ping -c 1 $servers | grep $? 
   if [ "$?" -eq "0" ]
   then
         echo "$servers reachable." >> upserver.txt
      else
            echo "$servers is unreachable." >> downserver.txt
         fi
done
clear
echo "######################"
echo "These servers are up: "
echo "######################"
echo""
cat ~/scripts/upserver.txt
echo "--------------------"
echo "#########################"
echo "The follwing are down: "
echo "#########################"
echo ""
cat ~/scripts/downserver.txt
echo "----------------------"
