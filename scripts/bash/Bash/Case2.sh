#!/bin/bash

VAR='2'

case $VAR in
   '1' | 'one' )
      echo 'One!' ;;
   '2' | 'two' ) 
      echo 'Two!' ;;
   '3' | 'three' ) 
      echo 'Three!' ;;
   * )
      echo 'Unknown Number' ;;
esac
