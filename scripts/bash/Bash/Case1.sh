#!/bin/bash

VAR=2

case $VAR in
   1 )
      echo 'One!' ;;
   2 ) 
      echo 'Two!' ;;
   3 ) 
      echo 'Three!' ;;
   * )
      echo 'Unknown Number' ;;
esac
