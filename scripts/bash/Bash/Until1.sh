#!/bin/bash

COUNTER=0
until [[ $COUNTER > 5 ]]
do
  let "COUNTER+=1"
  echo $COUNTER
done
