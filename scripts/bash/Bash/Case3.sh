#!/bin/bash

VAR=2

case $VAR in
   [123] )
      echo '123' ;;
   [234] ) 
      echo '234' ;;
   [345] ) 
      echo '345' ;;
   * )
      echo 'Out of range' ;;
esac
