#!/bin/bash

read -p "Hit 'x' key: " -n 1 INPUT

echo

if [[ $INPUT = 'x' ]]; then
   echo 'Thanks!'
else
   echo 'Not an x!'
fi
