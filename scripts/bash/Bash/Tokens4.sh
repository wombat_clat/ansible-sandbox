#!/bin/bash

echo "Num of Tokens: $#"

COUNT=0

for ARG in $@
do
   let "COUNT+=1"
   echo "Argument $COUNT: "  $ARG
done
