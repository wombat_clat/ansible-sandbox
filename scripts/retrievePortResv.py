#!/usr/bin/env python

import os, sys, time, smtplib
import pdb, json, re, csv, pdb
import Tkinter
import RdamDataManager, mySendmail
import gspread, itertools, stLogging
from oauth2client.client import SignedJwtAssertionCredentials

stLogging.startLogging(os.path.basename(__file__))

titleList = ['chassisName', 'chassisSN', 'lineCardId', 'lineCardSN', 'lineCardType', 'portId', 'IxAppResv', 'portStat', 'comment', 'ownerResv', 'project']

def writeToSheet(portList, wks, sheetName):
   ##reset data
   print('reset data to worksheet %s' % sheetName)
   cell_list = wks.range('A1:%s2000' % chr(ord('A') + len(portList[0]) - 1))
   for cell in range(len(cell_list)):
      cell_list[cell].value = ''
   wks.update_cells(cell_list)
   ##write new data
   print('write data to worksheet %s' % sheetName)
   portList1 = list(itertools.chain.from_iterable(portList))
   cell_list = wks.range('A1:%s%s' % (chr(ord('A') + len(portList[0]) - 1), len(portList)))
   for cell in range(len(cell_list)):
      cell_list[cell].value = portList1[cell]
   wks.update_cells(cell_list)

def getLinkOwner():
   if tcl.eval('port cget -linkState') == '1': # '1' means port is up
      portStat = 'up'
   else:
      portStat = 'down'
   IxAppResv = tcl.eval('port cget -owner')
   #print IxAppResv
   if IxAppResv == '':
      IxAppResv = 'free'
      IxAppResvComment = ''
   else:
      regExp = r'IxNetwork\/(IXS|ixs)([0-9a-zA-z\.]+)'
      match = re.search(regExp, IxAppResv)
      #print match
      if match is None:
         IxAppResvComment = ''
      else:
         #print match.group(2)
         IxAppResv = 'ixs' + match.group(2)
         dut = RdamDataManager.getDut(IxAppResv)
         if dut == None:
            IxAppResvComment = IxAppResv
         else:
            IxAppResv = dut.owner
            IxAppResvComment = dut.grabComment
   return (IxAppResv, portStat, IxAppResvComment)

def setApiEnv(chassisVersion):
   os.environ['IXIA_HOME'] = chassisList['chassisVersion'][chassisVersion]["IXIA_HOME"]
   os.environ['IXIA_VERSION'] = chassisList['chassisVersion'][chassisVersion]['IXIA_VERSION']
   os.environ['TCLLIBPATH'] = chassisList['chassisVersion'][chassisVersion]['TCLLIBPATH']
   sys.path.append(chassisList['chassisVersion'][chassisVersion]['PATH'])

def solutionAdded(IxAppResv):
   for i in range(1, len(lineCardSNList1)):
      if (lineCardSNList1[i] == lineCardSN):
         ownerResv = 'solution test ownerResv, ' + owner1[i]
         project = project1[i]
         if IxAppResv == 'free':
            IxAppResv = IxAppResv + ', (reserved by solution test team)'
            break
         else:
            break
      else:
         ownerResv = ''
         project = ''
     
   return (IxAppResv, ownerResv, project)
   

##get chassis from json
#json_data=open('chassisList_debug.json')
json_data=open('chassisList.json')
chassisList = json.load(json_data)
json_data.close()

##init data
chassisId = 1 #always 1
portListAll = [] #creat blank list
portListAll.append(titleList)
initChassisVersion = ''
lineCardDic = {}

## open json file
json_key = json.load(open('4680ff3a74c5.json'))
scope = ['https://spreadsheets.google.com/feeds']
credentials = SignedJwtAssertionCredentials(json_key['client_email'], json_key['private_key'], scope)

## read solution sheet
gc = gspread.authorize(credentials)
sh1 = gc.open(chassisList['fileName1'])
sheetName = 'Sheet1'
wks1 = sh1.worksheet(sheetName)
lineCardSNList1 = wks1.col_values(4)
#print lineCardSNList1
owner1 = wks1.col_values(6)
#print owner1
project1 = wks1.col_values(7)
#print project1

##collect data and save to list
for chassisName in chassisList['chassisName'].keys():
   if (initChassisVersion != chassisList['chassisName'][chassisName]):
      setApiEnv(chassisList['chassisName'][chassisName])
      initChassisVersion = chassisList['chassisName'][chassisName]
      #print ('chassisName is %s' % chassisName)
      #print ('chassis TCL version is %s' % initChassisVersion)
      tcl = Tkinter.Tcl()
      tcl.eval('package req Ixia')

   print ('chassisName is %s' % chassisName)
   print ('chassis TCL version is %s' % initChassisVersion)
   tcl.eval('ixConnectToTclServer %s' % chassisName)
   tcl.eval('ixConnectToChassis %s' % chassisName)
   chassisSN = tcl.eval('clientSend $ixTclSvrHandle {puts $env(chassisSN)}')
   maxCardCount = int(tcl.eval('chassis cget -maxCardCount'))
   for lineCardId in range (1, maxCardCount+1):
      if tcl.eval('card get %s %s' % (chassisId, lineCardId)) == '0': #if card exist
         tmp = tcl.eval('card cget -serialNumber')
         regExp = r'Serial Number ([0-9a-zA-z\.]+)'
         lineCardSN = re.search(regExp, tmp).group(1)
         lineCardSN = str(int(lineCardSN))
         lineCardType = tcl.eval('card cget -typeName')
         ##
         lineCardExist = 0
         for key in lineCardDic:
            if key == lineCardType: ## card type exist, add 1
               lineCardDic[key] = lineCardDic[key] + 1
               lineCardExist = 1
         if lineCardExist == 0:
            lineCardDic[lineCardType] = 1
            lineCardExist = 1
         ##
         if (lineCardType == 'XM100GE4CXP+FAN+10GE' or lineCardType == 'XM100GE4QSFP28+ENH+25G'): # multis
            print('lineCardId %s, lineCardType %s' % (str(lineCardId), lineCardType))
            tcl.eval('set activePortList [card cget -activePortList]')
            resourceGroupLength = int(tcl.eval('llength $activePortList'))
            for resourceGroupId in range(resourceGroupLength):
               tcl.eval('set portList [lindex $activePortList %s]' % resourceGroupId)
               portId = int(tcl.eval('lindex $portList 0'))
               tcl.eval('port get %s %s %s' % (chassisId, lineCardId, portId))
               IxAppResv, portStat, IxAppResvComment = getLinkOwner()
               portId = resourceGroupId + 1 # loop portId with resourceGroupId
               IxAppResv, ownerResv, project = solutionAdded(IxAppResv)
               port = [str(chassisName), chassisSN.rstrip(), str(lineCardId), lineCardSN, lineCardType, str(portId), IxAppResv, portStat, str(IxAppResvComment), ownerResv, project]
               portListAll.append(port)
         elif lineCardType == 'FlexAP1040SQ': # 1x40g/4x10g
            print('lineCardId %s, lineCardType %s' % (str(lineCardId), lineCardType))
            tcl.eval('set activePortList [card cget -activePortList]')
            resourceGroupLength = int(tcl.eval('llength $activePortList'))
            for resourceGroupId in range(resourceGroupLength):
               tcl.eval('set portList [lindex $activePortList %s]' % resourceGroupId)
               portLength = int(tcl.eval('llength $portList'))
               if portLength == 1: ## if 1x40g
                  portId = int(tcl.eval('lindex $portList 0'))
                  tcl.eval('port get %s %s %s' % (chassisId, lineCardId, portId))
                  IxAppResv, portStat, IxAppResvComment = getLinkOwner()
                  port = [str(chassisName), chassisSN.rstrip(), str(lineCardId), lineCardSN, lineCardType, str(portId), IxAppResv, portStat, str(IxAppResvComment), ownerResv, project]
                  portListAll.append(port)
               else: ## if 4x10g
                  for port in range(portLength):
                     portId = int(tcl.eval('lindex $portList %s' % port))
                     tcl.eval('port get %s %s %s' % (chassisId, lineCardId, portId))
                     IxAppResv, portStat, IxAppResvComment = getLinkOwner()
                     IxAppResv, ownerResv, project = solutionAdded(IxAppResv)
                     port = [str(chassisName), chassisSN.rstrip(), str(lineCardId), lineCardSN, lineCardType, str(portId), IxAppResv, portStat, str(IxAppResvComment), ownerResv, project]
                     portListAll.append(port)
         else: ##regular linecard
            print('lineCardId %s, lineCardType %s' % (str(lineCardId), lineCardType))
            portId = 1
            while tcl.eval('port get %s %s %s' % (chassisId, lineCardId, portId)) == '0': #while port exist
               IxAppResv, portStat, IxAppResvComment = getLinkOwner()
               IxAppResv, ownerResv, project = solutionAdded(IxAppResv)
               port = [str(chassisName), chassisSN.rstrip(), str(lineCardId), lineCardSN, lineCardType, str(portId), IxAppResv, portStat, str(IxAppResvComment), ownerResv, project]
               portListAll.append(port)
               portId += 1
   print lineCardDic
      
## open 'port utilization port'
gc = gspread.authorize(credentials)
sh = gc.open(chassisList['fileName'])
#sh = gc.open('ixia port utilization debug') ## for debugging
wks = sh.worksheet('portListAll')

## read current table
portListAllOld = wks.get_all_values()

## update new table 
for port in portListAll:
   for portOld in portListAllOld:
      if (port[1] + port[2] + port[3] + port[5] == portOld[1] + portOld[2] + portOld[3] + portOld[5]): ## if entry exist in old table
         if port[6] == 'free':
            if 'free' not in portOld[6]: ## if port was reserved, then copy previous owner to current cell
               port[6] = ('free, (%s released port at %s)' % (portOld[6], time.strftime('%m-%d-%Y')))
            elif portOld[6] == 'free': ## record when port is free
               port[6] = ('free, (from %s)' % time.strftime('%m-%d-%Y'))
            else: ## already record when port is free, copy cell
               port[6] = portOld[6] 
         if portOld[9] != '': ## if port is reserved manually 
            port[9] = portOld[9]
         if port[7] == 'down': ## record when port is down
            if portOld[7] == ('down' or 'up'):
               port[7] = ('down, (from %s)' % time.strftime('%m-%d-%Y'))
            else: ## with from
               port[7] = portOld[7]
         break


## write to fundamental sheet
sheetName = 'portListAll'
wks = sh.worksheet(sheetName)
writeToSheet(portListAll, wks, sheetName)

## write to other sheet 
portListFree = []
port = titleList
portListFree.append(port)
for port in portListAll:
   if 'free' in port[6]:
      portListFree.append(port)
sheetName = 'portListFree'
wks = sh.worksheet(sheetName)
writeToSheet(portListFree, wks, sheetName)

portListResvDown = []
port = titleList
portListResvDown.append(port)
for port in portListAll:
   if (('free' not in port[6]) and ('down' in port[7])):
      portListResvDown.append(port)
sheetName = 'portListResvDown'
wks = sh.worksheet(sheetName)
writeToSheet(portListResvDown, wks, sheetName)

portListFreeUp = []
port = titleList
portListFreeUp.append(port)
for port in portListAll:
   if (('free' in port[6]) and (port[7] == 'up')):
      portListFreeUp.append(port)
sheetName = 'portListFreeUp'
wks = sh.worksheet(sheetName)
writeToSheet(portListFreeUp, wks, sheetName)

## lineCard Statistics
lineCardList = []
port = ['lineCardType', 'total']
lineCardList.append(port)
for key in lineCardDic:
   tmpList = [key, lineCardDic[key]]
   lineCardList.append(tmpList)
sheetName = 'lineCardStat'
wks = sh.worksheet(sheetName)
writeToSheet(lineCardList, wks, sheetName)


##sendmail
subject = 'ixia port reservation is updated @ ' + time.strftime("%Y-%m-%d-%H:%M:%S")
body = 'link is @\n https://docs.google.com/a/arista.com/spreadsheets/d/1mCQiYQCwUUkRGFPPN1RqpfhMFIGAf8MjYr_pe63GDKk/edit#gid=0\n'
mySendmail.mail(str(chassisList['mail']['From']), str(chassisList['mail']['To']), subject, body)



