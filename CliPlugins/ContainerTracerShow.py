import CliParser
import BasicCli
import CliModel
import ArnetModel
import json
import re
import TableOutput

from subprocess import call, check_output, CalledProcessError
import ContainerTracerLib

c = ContainerTracerLib.ContainerTracer()


def doContainerTracer():
        print("hi")

tokenLogout = CliParser.KeywordRule( 'dockertracer',
                                     helpdesc='Exit from the EXEC' )

BasicCli.UnprivMode.addCommand( ( tokenLogout, doContainerTracer ) )

class SwarmNode( CliModel.Model ):
   nodeName = CliModel.Str(help = 'Name of swarm node')
   containerName = CliModel.Str(help = 'Name of container')
   ports = CliModel.Str(help = 'List of ports' )
   localInterface = CliModel.Str(help = 'Interface node is connected to')
   containerId = CliModel.Str(help = 'Id of container')
   created = CliModel.Str(help = 'Date on which Container was created')
   status = CliModel.Str(help = 'Status of the Container')
   commands = CliModel.Str(help = 'Commands running')
   debug_msg = CliModel.Str(help = 'Debugging Messages')

class ContainertracerSwarm(CliModel.Model):

   output = CliModel.Dict(help="output string", valueType=SwarmNode)

   def render(self):
       # Process the display
       tableHeadings = ("NODE","CONTAINER\nNAME","PORTS","LOCAL\nINTERFACE")
       table = TableOutput.createTable(tableHeadings, tableWidth=94)
       f1 = TableOutput.Format( justify = "left", maxWidth=30)
       f1.noPadLeftIs( True )
       table.formatColumns(f1,f1,f1,f1)
       for node_id, swarmNode in self.output.items():
          table.newRow( swarmNode.nodeName, swarmNode.containerName, swarmNode.ports, swarmNode.localInterface )
       print table.output()
       if 'debug' in self.output:
          print self.output['debug']['debug_msg']

def getShowContainertracerSwarm( mode ):
       cmd = '/usr/bin/containertracer.linux -config_path=%s' \
            % ( c.container_conf )
       try:
          output = check_output([cmd], shell=True)
       except CalledProcessError as e:
          output = "ERROR: Check Eos or Swarm configuration:  " + str(e.output)
       return output

def fillSwarmNode( output ):
   outputNodes = dict()
   if output.startswith('ERROR'):
      swarmNode = SwarmNode()
      swarmNode.nodeName = ""
      swarmNode.containerName = ""
      swarmNode.ports = ""
      swarmNode.localInterface = ""
      swarmNode.containerId = ""
      swarmNode.created = ""
      swarmNode.status = ""
      swarmNode.commands = ""
      swarmNode.debug_msg = output
      outputNodes['debug'] = swarmNode

   else:
      for node in json.loads(output):
         swarmNode = SwarmNode()
         swarmNode.nodeName = node['NODE']
         swarmNode.containerName = node['CONTAINER NAME']
         swarmNode.ports = node['PORTS']
         swarmNode.localInterface = node['LOCAL INTERFACE']
         swarmNode.containerId = node['CONTAINER ID']
         swarmNode.created = node['CREATED']
         swarmNode.status = node['STATUS']
         swarmNode.commands = node['COMMANDS']
         swarmNode.debug_msg = ''
         outputNodes[node['CONTAINER ID']] = swarmNode
   return outputNodes

class ShowContainertracerSwarmCmd( object ):
    syntax = 'show containertracer swarm'
    data = { 'containertracer': 'name of the docker utility',
             'swarm': 'show all the containers on swarm'
             }
    def handler(self, mode, args ):
       output = getShowContainertracerSwarm(mode)
       cts = ContainertracerSwarm()
       with open(c.container_conf) as config:
          data = json.load(config)
       cts.output = fillSwarmNode( output )
       return cts

BasicCli.registerShowCommandClass(  ShowContainertracerSwarmCmd, cliModel=ContainertracerSwarm )


class ContainertracerSwarmDetail(CliModel.Model):
   eosUsername = CliModel.Str( help = 'username of the EOS device' )
   eosPassword = CliModel.Str( help = 'password')
   eosHost = ArnetModel.Ip4Address( help = 'hostname of the EOS device' )
   eosTransport = CliModel.Str( help ='transport protocol used' )
   output = CliModel.Dict(help="output string", valueType=SwarmNode)

   def render(self):
        # Process the display
        print "Arista Docker Swarm Extension: %s" % (ContainerTracerLib.dockerVersion( 'ContainerTracer' ) )
        print "\n\nEOS Configuration:"
        print "\t%s: %s" %("USERNAME", self.eosUsername)
        print "\t%s: %s" %("PASSWORD", "Set" if self.eosPassword else "Not Set")
        print "\t%s: %s" %("HOST", self.eosHost)
        print "\t%s: %s" %("TRANSPORT", self.eosTransport)

        print "\n\nSwarm Details:\n"
        ctr = 0
        for node_id,swarmNode in self.output.items():
           print "DETAILS FOR CONTAINER %s" %(swarmNode.containerName)
           print ""
           print "\t%s: %s" %("NODE", swarmNode.nodeName)
           print "\t%s: %s" %("CONTAINER NAME", swarmNode.containerName)
           print "\t%s: %s" %("CONTAINER ID", swarmNode.containerId)
           print "\t%s: %s" %("COMMANDS", swarmNode.commands)
           print "\t%s: %s" %("STATUS", swarmNode.status)
           print "\t%s: %s" %("CREATED", swarmNode.created)
           print "\t%s: %s" %("PORTS", swarmNode.ports)
           print "\t%s: %s" %("LOCAL INTERFACE", swarmNode.localInterface)
           print ""
           ctr = ctr + 1
        if 'debug' in self.output:
           print self.output['debug']['debug_msg']

class ShowContainertracerSwarmDetailCmd( object ):
    syntax = 'show containertracer swarm detail'
    data = { 'containertracer': 'name of the docker utility',
             'swarm': 'show all the containers on swarm',
	     'detail': 'show details about EOS and Swarm'
             }

    def handler(self, mode, args ):
       output = getShowContainertracerSwarm(mode)
       cts = ContainertracerSwarmDetail()
       with open(c.container_conf) as config:
          data = json.load(config)
       cts.eosUsername = data['EosConfig']['Username']
       cts.eosHost = data['EosConfig']['Hostname']
       cts.eosPassword = 'Set' if data['EosConfig']['Password'] else 'Not Set'
       cts.eosTransport = data['EosConfig']['Transport']
       cts.output = fillSwarmNode( output )
       return cts

BasicCli.registerShowCommandClass(  ShowContainertracerSwarmDetailCmd, cliModel=ContainertracerSwarmDetail )
