import CliParser
import BasicCli
import CliMode.ContainerTracer as CT
import Tracing
import IpAddr
import ContainerTracerLib
import base64

traceHandle = Tracing.Handle( "ContainerTracer" )
t0 = traceHandle.trace0

t0( 'Starting the program!!' )

# token declaration

tokenContainerTracer = CliParser.KeywordRule( 'containertracer',
                                              helpdesc='containertracer mode' )


stringPatternRule = CliParser.PatternRule( '.+', helpname='WORD',
                                           helpdesc='A single word' )

portNumberRule = CliParser.PatternRule( '[0-9]+', helpname='Port numner',
                                        helpdesc='A digit')

# containertracerlib object
cntr = ContainerTracerLib.ContainerTracer()


class ContainerTracerConfigMode( CT.ContainerTracerMode,
                                 BasicCli.ConfigModeBase ):
    name = 'containertracer'
    modeRule = CliParser.OrRule( )

    def __init__( self, parent, session ):
        t0( 'In init of DSIConfigMode' )
        CT.ContainerTracerMode.__init__( self )
        BasicCli.ConfigModeBase.__init__( self, parent, session )


# gets the containertracermode
def gotoContainerTracerMode( mode ):
    childMode = mode.childMode( ContainerTracerConfigMode )
    mode.session_.gotoChildMode( childMode )

BasicCli.GlobalConfigMode.addCommand(( tokenContainerTracer,
                                       gotoContainerTracerMode ))


class ContainerSwarmConfig( object ):
    syntax = """ swarm ip <ip-address> port <port>  """
    data = { 'swarm': 'docker manager',
             'ip': 'ip address',
             '<ip-address>': IpAddr.ipAddr,
             'port': 'port number',
             '<port>': portNumberRule
             }

    def handler( self, mode, args ):
        if args:
            cntr.updateConfig( 'swarm', args )

ContainerTracerConfigMode.addCommandClass( ContainerSwarmConfig )


class ContainerEosConfig( object ):
    syntax = """ Eos username <username> password [ 0 | 1 ] <password> host <host>
                 transport ( http | https )"""
    data = { 'Eos': 'Eos device',
             'username': 'username of the EOS device',
             '<username>': stringPatternRule,
             'password': 'password of the EOS device',
             '0': 'what follows is an encrypted password',
             '1': 'what follows is an non-encrypted password',
             '<password>': stringPatternRule,
             'host': 'hostname of the EOS device',
             '<host>': IpAddr.ipAddr,
             'transport': 'hostname of the EOS device',
             'http': 'http',
             'https': 'https'
             }

    def handler( self, mode, args ):
        if args:
            # if not mode.session_.startupConfig():
            if '1' in args:
                args[ '1' ] = ''
                args[ '0' ] = '0'
                args[ '<password>' ] = base64.b64encode( args[ '<password>' ] )
            if '1' not in args and '0' not in args:
                args[ '0' ] = '0'
                args[ '<password>' ] = base64.b64encode( args[ '<password>' ] )
            cntr.updateConfig( 'eos', args)

ContainerTracerConfigMode.addCommandClass( ContainerEosConfig )
